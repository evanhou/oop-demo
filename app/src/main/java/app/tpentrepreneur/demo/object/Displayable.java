package app.tpentrepreneur.demo.object;

/**
 * Created by evanhou on 2016/4/14.
 */
public interface Displayable {

    public String getTitle();
    public String getDate();
    public String getThumbnail();
}
