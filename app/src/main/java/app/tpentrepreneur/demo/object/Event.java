package app.tpentrepreneur.demo.object;

/**
 * Created by evanhou on 2016/4/14.
 */
public class Event implements Displayable {

    private String title;
    private String holder;
    private String startDate;
    private String endDate;
    private String thumbnail;

    public void setTitle(String title) {
        this.title = title;
    }

    public void setHolder(String holder) {
        this.holder = holder;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getHolder() {
        return holder;
    }

    @Override
    public String getTitle() {
        return "This is event " + title + " hold by "+holder;
    }

    @Override
    public String getDate() {
        return startDate + " - " + endDate;
    }

    @Override
    public String getThumbnail() {
        return thumbnail;
    }
}
