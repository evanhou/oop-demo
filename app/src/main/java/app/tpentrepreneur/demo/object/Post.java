package app.tpentrepreneur.demo.object;

import java.util.Calendar;
import java.util.Locale;

/**
 * Created by evanhou on 2016/4/14.
 */
public class Post implements Displayable {

    private String title;
    private Calendar date;
    private String thumbnail;

    public Post() {

    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDate(Calendar date) {
        this.date = date;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    @Override
    public String getTitle() {
        return "This is post "+title;
    }

    @Override
    public String getDate() {
        int Y = date.get(Calendar.YEAR);
        String M = date.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.ENGLISH);
        int d = date.get(Calendar.DATE);
        String w = date.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT, Locale.ENGLISH);
        return Y+" "+M+" "+d+" "+w;
    }

    @Override
    public String getThumbnail() {
        return thumbnail;
    }
}
