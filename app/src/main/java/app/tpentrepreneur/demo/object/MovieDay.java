package app.tpentrepreneur.demo.object;

/**
 * Created by evanhou on 2016/4/14.
 */
public class MovieDay extends Event {

    private String movieName;
    private int ticketPrice;

    public MovieDay() {

    }

    public String getMovieName() {
        return movieName;
    }

    public void setMovieName(String movieName) {
        this.movieName = movieName;
    }

    public int getTicketPrice() {
        return ticketPrice;
    }

    public void setTicketPrice(int ticketPrice) {
        this.ticketPrice = ticketPrice;
    }


    @Override
    public String getHolder() {
        return super.getHolder() + "大大";
    }



    public String getAnnouncement() {
        return "Hi All, 我們"+getDate()+"要去看"+getMovieName()+"，不能參加者請告知"+getHolder();
    }


}
