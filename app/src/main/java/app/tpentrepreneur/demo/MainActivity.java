package app.tpentrepreneur.demo;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.Calendar;

import app.tpentrepreneur.demo.object.Displayable;
import app.tpentrepreneur.demo.object.Event;
import app.tpentrepreneur.demo.object.MovieDay;
import app.tpentrepreneur.demo.object.Post;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /* Parent Class */
        Event event = new Event();
        event.setTitle("Bravebot 讀書會");
        event.setStartDate("2016/04/15 15:00");
        event.setEndDate("2016/04/15 17:00");
        event.setHolder("白菜");
        event.setThumbnail("http://www.facebook.com/event");

        /* Child Class */
        MovieDay movieDay = new MovieDay();
        movieDay.setTitle("Bravebot Movie Day");
        movieDay.setStartDate("2016/04/16 15:00");
        movieDay.setEndDate("2016/04/16 17:00");
        movieDay.setHolder("Linus");
        movieDay.setThumbnail("http://www.facebook.com/event");
        movieDay.setMovieName("美國隊長3");
        movieDay.setTicketPrice(500);


        System.out.println(movieDay.getAnnouncement());

        MovieDay anotherDay = new MovieDay(){
            @Override
            public String getAnnouncement() {
                return super.getAnnouncement() + "!!!";
            }
        };


        Post post = new Post();
        post.setTitle("OOP is fun");
        post.setDate(Calendar.getInstance());
        post.setThumbnail("http://www.google.com");




        // TODO : Polymorphism
        showList(event);
        showList(post);


        ArrayList<Object> list = new ArrayList<>();
        list.add(event);
        list.add(post);

        for (Object item : list) {
            showList((Displayable) item);
        }

    }

    public void showList(Displayable item) {
        System.out.println(item.getTitle());
        System.out.println(item.getDate());
        System.out.println(item.getThumbnail());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }
}
